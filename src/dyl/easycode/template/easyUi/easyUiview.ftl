<%@include file="/taglib.jsp"%>
<%@ page contentType="text/html; charset=UTF-8"%> 
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>${table.javaProperty}</title>
	<%@include file="/WEB-INF/views/common/easyUiRes.jsp"%>
	<#if hasUpload??>
	<%@include file="/WEB-INF/views/common/UploadRes.jsp"%>
	</#if>
</head>
<body>
		<div id="queryDiv" style="padding:5px;height:auto">
			<form id="searchForm">
				<div style="margin-bottom:5px">
					<#list table.baseColumns as c>
					<#if c.selectSql??>
	    			${c.remarks!c.javaProperty}:<select class="easyui-combobox"  type="text" name="${c.javaProperty}" panelHeight="auto">
	    				<option value="">--请选择--</option>
	    				<c:forEach items="<#noparse>${</#noparse>${c.javaProperty}List<#noparse>}</#noparse>" var="st">
	    					<#noparse><option value="${st.id}">${st.name}</option></#noparse>
	    				</c:forEach>
		    		</select>
	    			<#else>
	    			${c.remarks!c.javaProperty}:<input type="text"  name="${c.javaProperty}">
	    			</#if>
					</#list>
					<a href="#" class="easyui-linkbutton" onclick='javascript:searchData();'  iconCls="icon-search">查询</a>
					<#if hasEdit??>
					<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="intoAdd();">新增</a>
					<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="intoEdit();">修改</a>
					<a href="#" class="easyui-linkbutton" iconCls="icon-cancel"  onclick="deleteData();">删除</a>
					</#if>
					<#if hasUpload??>
					<a href="#" class="easyui-linkbutton" iconCls="icon-remove"  onclick="truncateData();">清空数据</a>
					<a href="#" class="easyui-linkbutton" iconCls="icon-save"  onclick="javascript:$('#uploadDiv').dialog('open');">上传数据</a>
					<a href="<#noparse>${res}</#noparse>/drTemplate/${table.javaProperty}.xls" class="easyui-linkbutton" >导入模板下载</a>
					</#if>
				</div>
			</form>
		</div>
	<table id="tableDiv"  url="${table.javaProperty}!find${table.className}List.do"  
		data-options="pagination:true,pageSize:20,rownumbers:true,toolbar:'#queryDiv',fit:true,fitColumns:true,method:'post'">
		<thead>
			<tr>
				<th data-options="field:'${table.primaryKey.javaProperty}',checkbox:true"></th>
				<#list table.baseColumns as c>
				<th data-options="field:'${c.javaProperty}'">${c.remarks!c.javaProperty}</th>
				</#list>
			</tr>
		</thead>
	</table>
	<#if hasEdit??>
	<div id="editDiv" class="easyui-dialog" title="修改数据" data-options="modal:true,closed:true,buttons:'#dlg-buttons',resizable:true" style="padding:10px;">
			<div style="padding:10px 60px 20px 60px">
		    <form id="editForm" method="post">
				<input type="hidden" name="${table.primaryKey.javaProperty}" />
		    	<table cellpadding="5"><!-- data-options="required:true" -->
					<#list table.baseColumns as c>
		    		<tr>
		    			<td>${c.remarks!c.javaProperty}:</td>
		    			<#if c.selectSql??>
		    			<td><select class="easyui-combobox"  type="text" name="${c.javaProperty}" data-options="required:true" panelHeight="auto">
		    				<c:forEach items="<#noparse>${</#noparse>${c.javaProperty}List<#noparse>}</#noparse>" var="st">
		    					<#noparse><option value="${st.id}">${st.name}</option></#noparse>
		    				</c:forEach>
			    		</select></td>
		    			<#else>
		    			<#if c.javaType !='Date'>
		    			<td><input class="easyui-textbox" type="text" data-options="required:true" name="${c.javaProperty}"/></td>
		    			<#else>
		    			<td><input class="easyui-datetimebox" editable="false" data-options="required:true" name="${c.javaProperty}"/></td>
		    			</#if>
		    			</#if>
		    		</tr>
					</#list>
		    	</table>
		    	
		    </form>
	    </div>
	</div>
	<#if hasUpload??>
	<div id="uploadDiv" class="easyui-dialog" title="文件上传" data-options="modal:true,closed:true,resizable:true" style="padding:10px;">
		   <div id="uploadFile"></div> 
		   <div id="p" class="easyui-progressbar" style="width:400px;"></div>
	</div>
	</#if>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="javascript:saveOrUpdateData();">保存</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#editDiv').dialog('close')">关闭</a>
	</div>
	</#if>
	<script type="text/javascript">
		//查询方法
		function searchData(){
			$("#tableDiv").datagrid({
				//封装form元素为json
				queryParams: form2Json('searchForm')
			});
		}
		<#if hasEdit??>
		function intoAdd(){
			//清空表单
			$('#editForm').form('clear');
			$('#editDiv').dialog('open');
		}
		//进入编辑弹窗
		function intoEdit(){
			if($('#tableDiv').datagrid('getSelections').length!=1){
				$.messager.alert('提示',"请选择一条记录");
				return;
			}
			$('#editForm').form('clear');
			var row = $('#tableDiv').datagrid('getSelected');
			//加载数据
			$('#editForm').form('load', '${table.javaProperty}!get${table.className}.do?${table.primaryKey.javaProperty}='+row.${table.primaryKey.javaProperty});
			$('#editForm').form({onLoadSuccess:function(){
				$('#editDiv').dialog('open');
			}}); 
		}
		//保存或修改数据
		function saveOrUpdateData(){
			if(!$("#editForm").form('validate'))return;
			getJsonByajaxForm("editForm","${table.javaProperty}!saveOrUpdate${table.className}.do",function(data){
				searchData();
				$('#editDiv').dialog('close');
			},true);
		}
		//删除数据
		function deleteData(){
			var ids = [];
			var rows = $('#tableDiv').datagrid('getSelections');
			if(rows.length==0){
				$.messager.alert('提示',"请至少选择一条记录");
				return;
			}
			$(rows).each(function(i,v){
				ids.push(v.${table.primaryKey.javaProperty});
			});
			$.messager.confirm('提示','确认删除所选择的列吗?',function(r){
			    if (r){
			    	getJsonDataByPost("${table.javaProperty}!delete${table.className}.do","${table.primaryKey.javaProperty}s="+ids.join(","),function(data){
						searchData();
					},true);
			    }
			});
		}
		</#if>
		<#if hasUpload??>
		//清空数据
		function truncateData(){
			$.messager.confirm('提示','确认清空数据吗,一旦确认数据将不可恢复?',function(r){
			    if (r){
			    	getJsonDataByPost("${table.javaProperty}!truncate${table.className}.do",'',function(data){
						searchData();
					},true);
			    }
			});
		}
		</#if>
		$(function(){
			//分页栏下方文字显示
			var pager = $('#tableDiv').datagrid().datagrid('getPager').pagination({});
			 <#if hasUpload??>
			$('#uploadFile').uploadify({
				<#noparse>
		        'swf'      : '${res}/uploadify/uploadify.swf',    //指定上传控件的主体文件
		        'uploader' : '${ctx}/uploadExcel.do',    //指定服务器端上传处理文件
		         </#noparse>
		        'buttonText':"点我上传",
		        "fileTypeExts":"*.xls;",
		        'multi':false,//能否选择多个稿件
		        'overrideEvents': ['onUploadProgress'],//方法重写
		        "fileSizeLimit":999999,//20M
		        'onUploadProgress': function(file,fileBytesLoaded,fileTotalBytes,
		           queueBytesLoaded,swfuploadifyQueueUploadSize){//上传进度发生变更时触发
		           	//修改自定义样式进度条
		           $('#p').progressbar('setValue',((fileBytesLoaded/fileTotalBytes)*100));
				},
		        'onUploadSuccess' : function(file,data,response){
		        	var data = eval("(" + data + ")");
		        	if(data.result){
		        		$.messager.alert('提示','成功上传'+data.successCount+"条数据");
		        		searchData();
		        	}else{
		        		$.messager.alert('提示','上传失败，请检查第'+data.errorLine+'条数据','error');
		        	}
		        	$('#uploadDiv').dialog('close');
		        },
		        'onFallback': function () {
		        	alert('您还没有安装上传插件');
		        },
		        'formData':{'tag' : '${table.javaProperty}'}
		    }); 
		   </#if>
		})
	</script>
</body>
</html>