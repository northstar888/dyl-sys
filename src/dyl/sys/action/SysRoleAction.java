package dyl.sys.action;

import java.math.BigDecimal;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import dyl.common.bean.R;
import dyl.common.exception.CommonException;
import dyl.common.util.Page;
import dyl.sys.Annotation.Auth;
import dyl.sys.Annotation.AuthAction;
import dyl.sys.bean.SysRole;
import dyl.sys.service.SysMenuServiceImpl;
import dyl.sys.service.SysRoleServiceImpl;
/**
 * 由dyl EasyCode自动生成
 * @author Dyl
 * 2017-04-05 15:42:37
 */
@Controller
public class SysRoleAction extends BaseAction{
	@Resource
	private JdbcTemplate jdbcTemplate;
	@Resource
	private SysRoleServiceImpl sysRoleServiceImpl;
	@Resource
	private SysMenuServiceImpl sysMenuServiceImpl;
	
	 /**
	 * 说明：进入主页面方法
	 * @return String
	 */
	@Auth
	@RequestMapping(value = "/sysRole!main.do")
	public String main(Page page,SysRole sysRole,HttpServletRequest request){
		try{
			request.setAttribute("sysRoleList",sysRoleServiceImpl.findSysRoleList(page, sysRole));
			request.setAttribute("sysRole", sysRole);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
		return "/sys/sysRole/sysRoleMain";
	}
	 /**
	 * 说明：添加或修改
	 * @return String
	 */
	@Auth
	@RequestMapping(value = "/sysRole!sysRoleForm.do")
	public String  sysRoleForm(BigDecimal id,HttpServletRequest request){
		try {
			if(id!=null){
				request.setAttribute("sysRole",sysRoleServiceImpl.getSysRole(id));
			}
		} catch (Exception e){
			throw new CommonException(request, e);
		}
		return "/sys/sysRole/sysRoleForm";
	}
	/**
	 * 说明：修改角色权限
	 * @return String
	 */
	@Auth(action=AuthAction.UPDATE)
	@RequestMapping(value = "/sysRole!auth.do")
	public String  auth(BigDecimal roleId,HttpServletRequest request){
		try {
			request.setAttribute("roleId", roleId);
		} catch (Exception e){
			throw new CommonException(request, e);
		}
		return "/sys/sysRole/sysRoleAuth";
	}
	/**
	 * 说明：更新或者插入sys_role
	 * @return int >0代表操作成功
	 */
	@Auth(action=AuthAction.ADD)
	@ResponseBody()
	@RequestMapping(value = "/sysRole!add.do")
	public R add(SysRole sysRole,String authRoles,String oldAuthRoles,HttpServletRequest request){
		try {
			sysRole.setCreator(getSysUser(request).getId());
			int ret = sysRoleServiceImpl.insertSysRole(sysRole,authRoles,oldAuthRoles);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	/**
	 * 说明：更新或者插入sys_role
	 * @return int >0代表操作成功
	 */
	@Auth(action=AuthAction.UPDATE)
	@ResponseBody()
	@RequestMapping(value = "/sysRole!update.do")
	public R update(SysRole sysRole,String authRoles,String oldAuthRoles,HttpServletRequest request){
		try {
			sysRole.setCreator(getSysUser(request).getId());
			int ret = sysRoleServiceImpl.updateSysRole(sysRole,authRoles,oldAuthRoles);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	/**
	 * 说明：根据主键删除表sys_role中的记录
	 * @return int >0代表操作成功
	 */
	@Auth(action=AuthAction.DELETE)
	@ResponseBody()
	@RequestMapping(value = "/sysRole!delete.do")
	public R deleteSysRole(String  dataIds,HttpServletRequest request){
		try {
			int[] ret = sysRoleServiceImpl.deleteSysRole(dataIds);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	/**
	 * 说明：根据主键删除表sys_role中的记录
	 * @return int >0代表操作成功
	 */
	@Auth(action=AuthAction.ADD)
	@ResponseBody()
	@RequestMapping(value = "/sysRole!saveAuth.do")
	public R saveAuth(String  authRoleKinds,BigDecimal roleId,HttpServletRequest request){
		try {
			return returnByDbRet(sysRoleServiceImpl.saveAuth(authRoleKinds,roleId));
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
}
