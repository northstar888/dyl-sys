package dyl.sys.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.lingala.zip4j.io.ZipOutputStream;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import dyl.sys.bean.SysUser;
import dyl.sys.util.DateUtil;
import dyl.sys.util.FileUtils;
@Controller
public class DownloadAction extends BaseAction { 
	static Logger logger = Logger.getLogger(DownloadAction.class); 
	@Resource
	private JdbcTemplate jdbcTemplate;
	
	
	/**
	 * 下载单个文件
	 * @param fileName
	 * @param name
	 * @param request
	 * @param response
	 * @throws Exception
	 */
    @RequestMapping(value = "/download.do")
	public void downfile(String fileName,String name,HttpServletRequest request,HttpServletResponse response) throws Exception{
    	BufferedInputStream bis = null;
	    BufferedOutputStream bos = null;
	    OutputStream fos = null;
	    InputStream fis = null;
	    if(null != fileName){
	    	fileName = fileName.replaceAll("\\.\\.\\/|\\.\\/", "");
	    	String path = FileUtils.filePathProductor(fileName);
	            File downFiles = new File(path);
	            if(!downFiles.exists()){
	            	response.getWriter().print("文件不存在");
	                return;
	            }
	            try {
	                fis = new FileInputStream(downFiles);
	                bis = new BufferedInputStream(fis);
	                fos = response.getOutputStream();
	                bos = new BufferedOutputStream(fos);
	                if(StringUtils.isEmpty(name))name=fileName;
	                setFileDownloadHeader(request,response,name);
	                int byteRead = 0;
	                byte[] buffer = new byte[8192];
	                while((byteRead=bis.read(buffer,0,8192))!=-1){
	                    bos.write(buffer,0,byteRead);
	                }
	                bos.flush();
	                fis.close();
	                bis.close();
	                fos.close();
	                bos.close();
	            } catch (Exception e) {
	                log.info("下载失败了......");
	            }
	    }
	}
	/**
	 * 设置让浏览器弹出下载对话框的Header.
	 * 根据浏览器的不同设置不同的编码格式  防止中文乱码
	 * @param fileName 下载后的文件名.
	 */
	public static void setFileDownloadHeader(HttpServletRequest request,HttpServletResponse response, String fileName) {
	    try {
	    	//fileName = new String(fileName.getBytes("iso8859-1"),"UTF-8");
	        //中文文件名支持
	        String encodedfileName = null;
	        String agent = request.getHeader("USER-AGENT");
	        //IE11 使用 Gecko, 不大于10使用 MSIE
			if(null != agent && (-1 != agent.indexOf("MSIE") || -1 != agent.indexOf("like Gecko"))){//IE
	            encodedfileName = java.net.URLEncoder.encode(fileName,"UTF-8");
	        }else if(null != agent && -1 != agent.indexOf("Mozilla")){
	            encodedfileName = new String (fileName.getBytes("UTF-8"),"iso-8859-1");
	        }else{
	            encodedfileName = java.net.URLEncoder.encode(fileName,"UTF-8");
	        }
	        response.setHeader("Content-Disposition", "attachment; filename=\"" + encodedfileName + "\"");
	    } catch (UnsupportedEncodingException e) {
	        e.printStackTrace();
	    }
	}
} 
